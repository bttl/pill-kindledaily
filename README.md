# Kindle Daily Deals

This is a basic library to pull the latest Kindle Daily Deals from their
website and present them in an easy to aggregate format. Storage is handled
using a [JSON][] file only.  No database required.  Simple, readable, awesomle.

Working product: `Coming Soon`

[ ![Codeship Status for bttl/pill-kindledaily](https://www.codeship.io/projects/bfba1f60-3775-0131-84de-022dc370d557/status?branch=master)](https://www.codeship.io/projects/9948)

## Usage:

```python
>>> import deals
>>> data = deals.grabDeals()
>>> alldeals, newdeals = deals.buildData(data, loc='./', filename='data.json')
>>> deals.writeJSON(newdeals, loc='./', filename='deals.json')
>>> deals.writeRSS(alldeals, loc='./', filename='deals.rss')
>>> deals.writeXML(newdeals, loc='./', filename='deals.xml')
```

### `grabDeals()`

Acquires the data from Amazon. Returns a dictionary object.

### `buildData(data, loc='./', filename='data.json')`

Organizes everything taken from the grabDeals function.  Writes to `data.json`
and returns two dictionary objects.

1. Seven days worth of deals.
2. Only the new deals. (Empty dict if none.)

### `writeJSON(data, loc='./', filename='deals.json')`

Writes your output JSON file for use in your tool.

### `writeRSS(data, loc='./', filename='deals.rss')`

Writes your output [RSS 2.0][] file for use in your tool|aggregator.

### `writeXML(data, loc='./', filename='deals.xml')`

Writes your output [XML][] file for use in your tool.

## TODO

- Add more output types
    - RSS 0.91, 0.92 _(Maybe...)_
    - Atom
    - Simple [XML][] [Done!](https://bitbucket.org/bttl/pill-kindledaily/commits/e530c5e70c1bb77eb29e9e162220d534ec412ebd) or Verbose SOAP-capable XML
    - CSV _(Maybe...)_
- Use Firebase (Maybe...)
- Create example run script


[RSS 2.0]: http://cyber.law.harvard.edu/rss/rss.html
[JSON]: http://www.json.org/
[XML]: http://www.w3.org/TR/REC-xml/
[ATOM]: http://www.atomenabled.org/developers/syndication/atom-format-spec.php
