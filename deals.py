"""Kindle's Daily Deals
http://github.com/traeblain/kindledeals.py

Kindle's Daily Deals grabs the latest daily deal from Amazon.com and presents
the data in an easy to access manner.

Dependencies include:
  Requests>=0.14.1
  PyQuery>=1.2.4

Current outputs include:
  JSON        -> writeJSON()
  RSS 2.0     -> writeRSS()
  XML 1.0     -> writeXML()

License:

Copyright (c) 2012-2013, Trae Blain
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of Trae Blain nor BoTTLe Tools nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
import requests
import re
import os
from pyquery import PyQuery as pq
import json
from datetime import datetime as dt
from datetime import timedelta
from lxml import etree


__version__ = "0.4.2"
__author__ = "Trae Blain <http://traebla.in/>"
__copyright__ = "Copyright (c) 2012-2013 Trae Blain"
__license__ = "Modified BSD"


def getDetails(url):
  """Used to process the actual product pages to find details such as:
      Title of the Book
      Author of the Book (including editors if found)
      Current "Deal" Price of the Book.

  Input is the product URL.

  Output is [Book title, Author, Current Deal Price]
  """
  r = requests.get(url)
  p = pq(r.text)

  title = p("#btAsinTitle").text()
  author = p("#btAsinTitle").parent().next().text()
  price = p(".priceLarge").eq(0).text()

  return title, author, price


def grabDeals():
  """Primary function.  Used to grab the data from the Amazon page that lists
  Kindle's Daily Deal.  This creates a dict organized by a pseudo-category
  to be disseminated utilizing other functions.

  No input is given as the Amazon page is hard coded and constant.

  Ouput is a dict of found book deals.
  """
  urlObject = requests.get('http://www.amazon.com/gp/feature.html?\
    ie=UTF8&docId=1000677541')
  q = pq(urlObject.text)
  toptableData = q('.amabot_center').find('table').eq(0)
  bottomtableData = q('.amabot_center').find('table').eq(1)
  tableData = toptableData('td') + bottomtableData('td')
  dealContents = []
  for i in range(len(tableData)):
    td = tableData.eq(i).html()
    if td is not None:
      dealContents.append(td)
  theDeals = {}
  count = 0
  for i in dealContents:
    if len(i) < 150:
      if "<br clear" not in i[:9]:
        item = 'key{0}'.format(count)
        theDeals[item] = {}
        count = count + 1
        theDeals[item]['type'] = i
  count = 0
  for i in dealContents:
    if len(i) >= 150:
      item = 'key{0}'.format(count)
      count = count + 1
      theDeals[item]['content'] = i
  urlRe = re.compile("(/gp/product/([A-Za-z0-9/]+))")
  #print json.dumps(theDeals)
  for i in theDeals:
    contentDig = pq(theDeals[i]['content'])
    typeDig = pq(theDeals[i]['type'])
    url = contentDig.find('a').eq(0).attr['href']
    theDeals[i]['url'] = u'http://www.amazon.com{0}?ie=UTF8\
&tag=traeblain-20'.format(url)
    theDeals[i]['asin'] = url[-10:]
    for num, item in enumerate(contentDig.find('a')):
      contentDig.find('a').eq(num).attr['href'] = ''.join(
        ['http://www.amazon.com', 
          contentDig.find('a').eq(num).attr['href'], 
          '?ie=UTF8&tag=traeblain-20'])
    theDeals[i]['content'] = contentDig.html()
    theDeals[i]['type'] = typeDig.text()
    [theDeals[i]['title'],
     theDeals[i]['author'],
     theDeals[i]['price']] = getDetails(theDeals[i]['url'])

    if not theDeals[i]['title']:
      theDeals[i]['title'] = typeDig.text()

    for item in theDeals[i]:
      if not theDeals[i][item]:
        theDeals[i][item] = u'---'

  return theDeals


def buildData(data, loc='./', filename='data.json'):
  """Builds and organizes the relavant data, ensures it isn't duplicating
  content, and timestamps the processing.  This data is what is used to
  write all other outputs.

  Inputs are data taken from grabDeals(), the location of data file
    (Default "./"), and the file data file itself in JSON format.

  Output is first dict with 7 days of data, second with new data.
  """
  if not os.path.exists(os.path.join(os.path.abspath(loc), filename)):
    newFile = open(os.path.join(os.path.abspath(loc), filename), 'w')
    newFile.write('{}')
    newFile.close()

  existing = open(os.path.join(os.path.abspath(loc), filename), 'r')
  allData = json.loads(existing.read())
  existing.close()
  newdata = {}
  for i, item in enumerate(data):
    if data[item]['asin'] not in allData:
      thetime = dt.now() + timedelta(seconds=i)
      allData[data[item]['asin']] = {
        'title': data[item]['title'],
        'author': data[item]['author'],
        'description': data[item]['content'],
        'link': data[item]['url'],
        'category': data[item]['type'][:-11],
        'price': data[item]['price'],
        'pubDate': thetime.strftime("%a, %d %b %Y %H:%M:%S CST")
      }
      newdata[data[item]['asin']] = allData[data[item]['asin']]
  toDelete = []
  for item in allData:
    itemdate = dt.strptime(allData[item]['pubDate'],
                           '%a, %d %b %Y %H:%M:%S CST')
    if itemdate < dt.now() - timedelta(days=7):
      toDelete.append(item)
  for item in toDelete:
    del allData[item]
  newFile = open(os.path.join(os.path.abspath(loc), filename), 'w')
  newFile.write(json.dumps(allData, sort_keys=True, indent=2))
  newFile.close()
  return allData, newdata


def writeJSON(data, loc='./', filename='deals.json'):
  """Writes JSON data.  This contains all data gathered and stored to date.

  Input is data from buildData(), location of JSON file. (Default "./"), and
    filename to store passed data. (Default "deals.json")

  Output is JSON file in location specified.
  """
  if data:
    wfile = open(os.path.join(os.path.abspath(loc), filename), 'w')
    wfile.write(json.dumps(data, sort_keys=True, indent=2))
    wfile.close()
  return


def writeRSS(data, loc='./', filename='deals.rss'):
  """Write RSS 2.0 data.  This writes the latest 16 entries, sorted by date,
  truncating any additional data.

  Input is data from buildData() and location of RSS file. (Default "./"),
    and filename to store passed RSS data. (Default "deals.rss")

  Output is RSS file in location specified.
  """
  d = sorted(data.items(),
             key=lambda x: dt.strptime(x[1]['pubDate'],
                                       '%a, %d %b %Y %H:%M:%S CST'),
             reverse=True)
  now = dt.now().strftime("%a, %d %b %Y %H:%M:%S CST")

  tree = etree.Element('rss', version="2.0")
  channel = etree.SubElement(tree, 'channel')
  etree.SubElement(channel, 'title').text = "Kindle Daily Deals"
  etree.SubElement(channel, 'link').text = "http://bt.tl/pill/kindle/"
  etree.SubElement(channel, 'description').text = "Feed of Kindle's Daily \
Deals brought to you by Trae Blain.  http://traebla.in/ http://bt.tl/"
  etree.SubElement(channel, 'language').text = "en-us"
  etree.SubElement(channel, 'pubDate').text = now
  etree.SubElement(channel, 'lastBuildDate').text = now
  etree.SubElement(channel, 'docs').text = "http://bt.tl/pill/kindle/"
  etree.SubElement(channel, 'managingEditor').text = "trae@traeblain.com"
  etree.SubElement(channel, 'webMaster').text = "trae@traeblain.com"

  for item in d[:16]:
    subItem = etree.SubElement(channel, 'item')
    etree.SubElement(subItem, 'title').text = item[1]['title']
    etree.SubElement(subItem, 'author').text = item[1]['author']
    etree.SubElement(subItem, 'link').text = item[1]['link']
    etree.SubElement(subItem, 'category').text = item[1]['category']
    etree.SubElement(subItem, 'description').text = etree.CDATA(
                                                  item[1]['description']
                                                  .replace('\n', ''))
    etree.SubElement(subItem, 'pubDate').text = item[1]['pubDate']
    etree.SubElement(subItem, 'guid').text = item[0]

  wfile = open(os.path.join(os.path.abspath(loc), filename), 'w')
  wfile.write(etree.tostring(tree,
                             pretty_print=True,
                             xml_declaration=True,
                             encoding='utf-8'))
  wfile.close()
  return


def writeXML(data, loc='./', filename='deals.xml'):
  if not data:
    return

  tree = etree.Element('deals')

  for item in data:
    subItem = etree.SubElement(tree, 'item')
    etree.SubElement(subItem, 'asin').text = item
    etree.SubElement(subItem, 'title').text = data[item]['title']
    etree.SubElement(subItem, 'author').text = data[item]['author']
    etree.SubElement(subItem, 'link').text = data[item]['link']
    etree.SubElement(subItem, 'category').text = data[item]['category']
    etree.SubElement(subItem, 'description').text = etree.CDATA(
                                                  data[item]['description']
                                                  .replace('\n', ''))
    etree.SubElement(subItem, 'pubDate').text = data[item]['pubDate']
  tree.append(etree.Comment("Brought to you by http://traebla.in/ http://bt.tl/"))

  wfile = open(os.path.join(os.path.abspath(loc), filename), 'w')
  wfile.write(etree.tostring(tree,
                             pretty_print=True,
                             xml_declaration=True,
                             encoding='utf-8'))
  wfile.close()
  return
