#!/usr/bin/env python
import sys
import os
location = ''.join([os.getcwd(), '/'])
sys.path.append(location)
import deals
data = deals.grabDeals()
alldeals, newdeals = deals.buildData(data, loc=location, filename='kindle.data')
deals.writeJSON(newdeals, loc=location, filename='kindle.json')
deals.writeRSS(alldeals, loc=location, filename='kindle.rss')
deals.writeXML(newdeals, loc=location, filename='kindle.xml')

data = deals.grabDeals()
alldeals, newdeals = deals.buildData(data, loc=location, filename='kindle.data')
deals.writeJSON(newdeals, loc=location, filename='kindle.json')
deals.writeRSS(alldeals, loc=location, filename='kindle.rss')
deals.writeXML(newdeals, loc=location, filename='kindle.xml')

# Check  filesize to make sure they aren't empty
jsonsize = os.path.getsize(''.join([location, 'kindle.json']))
xmlsize = os.path.getsize(''.join([location, 'kindle.xml']))

if jsonsize < 5:
  raise Exception("JSON is empty...")

if xmlsize < 125:
  raise Exception("XML is empty...")